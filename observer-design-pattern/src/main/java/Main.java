public class Main {

    public static void main(String[] args) {

        NewsPaper nigde51 = new Nigde51();

        Subscribe customerOne = new Customer();
        customerOne.addSubscribe(nigde51);

        Subscribe customerTwo = new Customer();
        customerTwo.addSubscribe(nigde51);

        nigde51.sendInfoSubscribe();

        System.out.println("-------");

        customerOne.cancelSubscribe();

        nigde51.sendInfoSubscribe();
    }
}
