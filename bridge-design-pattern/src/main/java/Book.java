public class Book extends Stock {

    public Book(Production production) {
        super(production);
    }

    public void produce() {
        getProduction().produceBook();
    }
}
