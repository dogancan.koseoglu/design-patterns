import java.io.FileOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;

public class SerializeStrategy implements Strategy {

    public void save(Bean bean) {

        try {

            ObjectOutput outputObject = new ObjectOutputStream(new FileOutputStream(System.getProperty("user.dir") + "/src/bean.ser"));
            outputObject.writeObject(bean);
            outputObject.close();

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
