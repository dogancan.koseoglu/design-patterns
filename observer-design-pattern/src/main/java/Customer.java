public class Customer implements Subscribe {

    NewsPaper newsPaper;

    public NewsPaper getNewsPaper() {
        return newsPaper;
    }

    public void setNewsPaper(NewsPaper newsPaper) {
        this.newsPaper = newsPaper;
    }

    public void update() {
        System.out.println("Info was came.");
    }

    public void cancelSubscribe() {
        getNewsPaper().removeScribe(this);
    }

    public void addSubscribe(NewsPaper newsPaper) {
        setNewsPaper(newsPaper);
        newsPaper.addSubscribe(this);
    }
}
