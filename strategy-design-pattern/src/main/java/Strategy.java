public interface Strategy {

    void save(Bean bean);
}
