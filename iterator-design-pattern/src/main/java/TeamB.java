public class TeamB implements Team {

    Player[] playerList = new Player[5];

    public TeamB() {
        playerList[0] = new Player("f", 6);
        playerList[1] = new Player("g", 7);
        playerList[2] = new Player("h", 8);
        playerList[3] = new Player("i", 9);
        playerList[4] = new Player("j", 10);
    }

    public Iterator getIterator() {
        return new ArrayIterator(playerList);
    }
}
