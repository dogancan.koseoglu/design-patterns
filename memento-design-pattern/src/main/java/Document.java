import java.util.ArrayList;

public class Document {

    ArrayList<String> rows = new ArrayList<String>();

    public ArrayList<String> getRows() {
        return rows;
    }

    public void setRows(ArrayList<String> rows) {
        this.rows = rows;
    }

    public void addRow(String row) {
        getRows().add(row);
    }

    public void removeRow(int index) {
        getRows().remove(index);
    }

    public Memento createMemento() {
        return new Memento(getRows().toArray());
    }

    public void setMemento(Memento memento) {

        getRows().clear();

        for (int i = 0; i < memento.getElements().length; i++) {
            String row = memento.getElements()[i].toString();
            getRows().add(row);
        }
    }

    public String toString() {

        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < getRows().size(); i++) {

            stringBuilder.append(getRows().get(i)).append("\n");
        }

        return stringBuilder.toString();
    }
}
