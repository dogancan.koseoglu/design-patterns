public class FacadeShapeMaker {

    private static FacadeShapeMaker facadeShapeMaker = new FacadeShapeMaker();

    private FacadeShapeMaker() {
    }

    public static FacadeShapeMaker getFacadeShapeMaker() {
        return facadeShapeMaker;
    }

    public Shape getFacade(String shapeType) {

        Shape shape = null;

        if (shapeType.equals("c")) {
            shape = new Circle();
        } else if (shapeType.equals("s")) {
            shape = new Square();
        } else if (shapeType.equals("r")) {
            shape = new Rectangle();
        }

        return shape;
    }
}
