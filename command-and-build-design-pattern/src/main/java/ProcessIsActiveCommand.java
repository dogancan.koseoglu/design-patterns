/**
 * author: dkoseoglu
 * date: 28.04.2021 21:37
 **/
public class ProcessIsActiveCommand implements ProcessCommand {

    @Override
    public void execute(ProcessBuilder processBuilder) {
        processBuilder.getProcess().setName("test process");
        processBuilder.setActive(true);
    }

}
