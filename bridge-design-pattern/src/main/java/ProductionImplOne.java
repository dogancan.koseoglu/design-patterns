public class ProductionImplOne implements Production {

    private FactoryOne factoryOne = new FactoryOne();

    public void producePencil() {
        factoryOne.produceBluePencil();
    }

    public void produceBook() {
        factoryOne.produceBlueBook();
    }
}
