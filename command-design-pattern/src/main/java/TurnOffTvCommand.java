public class TurnOffTvCommand implements Command {

    private TV tv = null;

    public TurnOffTvCommand(TV tv) {
        this.tv = tv;
    }

    public void execute() {
        this.tv.turnOffTv();
    }
}
