public class Circle implements Shape {

    private String color;

    private Integer x;

    private Integer y;

    private Integer radius;

    public Circle(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    public Integer getY() {
        return y;
    }

    public void setY(Integer y) {
        this.y = y;
    }

    public Integer getRadius() {
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }

    public void draw() {
        System.out.println("Circle draw: x-> " + getX() + " y-> " + getY() + " radius-> " + getRadius() + " color-> " + getColor());
    }
}
