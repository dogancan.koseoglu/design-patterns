import java.util.Date;

/**
 * author: dkoseoglu
 * date: 28.04.2021 21:33
 **/

public class Process {

    private String name;

    private Date date;

    public Process() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
