public class RemoteControl {

    public Command[] commandArray = new Command[2];

    public RemoteControl() {

        TV tv = new TV();

        commandArray[0] = new TurnOnTvCommand(tv);
        commandArray[1] = new TurnOffTvCommand(tv);
    }

    public void takeCommand(int command) {
        commandArray[command].execute();
    }
}
