public class Main {

    public static void main(String[] args) {

        Computer computer = new Computer();

        Item monitor = new Monitor();
        Item keyboard = new Keyboard();
        Item ram = new Keyboard();

        computer.addItem(monitor);
        computer.addItem(keyboard);
        computer.addItem(ram);

        System.out.println("monitor price -> " + monitor.getPrice());
        System.out.println("keyboard price -> " + keyboard.getPrice());
        System.out.println("ram price -> " + ram.getPrice());

        System.out.println("---------------------------------------------");

        System.out.println("computer price -> " + computer.getPrice());


    }
}
