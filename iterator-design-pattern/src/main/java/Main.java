public class Main {

    public static void main(String[] args) {

        TeamA teamA = new TeamA();
        Iterator teamAIterator = teamA.getIterator();

        while (teamAIterator.hasNext()) {
            Player playerA = (Player) teamAIterator.next();
            System.out.println("name: " + playerA.name + " number: " + playerA.number);
        }

        System.out.println("-------");

        TeamB teamB = new TeamB();
        Iterator teamBIterator = teamB.getIterator();

        while (teamBIterator.hasNext()) {
            Player playerB = (Player) teamBIterator.next();
            System.out.println("name: " + playerB.name + " number: " + playerB.number);
        }

    }
}
