/**
 * author: dkoseoglu
 * date: 28.04.2021 21:37
 **/
public interface ProcessCommand {
    void execute(ProcessBuilder processBuilder);
}
