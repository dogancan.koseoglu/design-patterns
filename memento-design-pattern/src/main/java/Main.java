public class Main {

    public static void main(String[] args) {

        Document document = new Document();

        document.addRow("first row");
        document.addRow("second row");
        document.addRow("third row");

        System.out.println("1.State");
        System.out.println(document.toString());

        Memento memento = document.createMemento();

        document.removeRow(2);
        document.addRow("forth row");

        System.out.println("-------");
        System.out.println("2.State");
        System.out.println(document.toString());

        document.setMemento(memento);

        System.out.println("-------");
        System.out.println("3.State");
        System.out.println(document.toString());
    }
}
