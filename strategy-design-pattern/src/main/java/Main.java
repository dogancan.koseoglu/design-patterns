public class Main {

    public static void main(String[] args) {

        Bean bean = new Bean();
        bean.setCounter(10);
        bean.setName("Strategy");

        FileManager.instance().saveBean(bean);
    }
}
