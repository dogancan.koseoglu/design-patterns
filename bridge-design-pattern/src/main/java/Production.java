public interface Production {

    public void producePencil();

    public void produceBook();
}
