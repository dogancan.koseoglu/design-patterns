public abstract class Stock {

    private Production production;

    public abstract void produce();

    public Stock(Production production) {
        this.production = production;
    }

    public Production getProduction() {
        return production;
    }

    public void setProduction(Production production) {
        this.production = production;
    }
}
