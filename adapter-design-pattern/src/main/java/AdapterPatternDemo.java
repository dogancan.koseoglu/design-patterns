public class AdapterPatternDemo {

    public static void main(String[] args) {

        AudioPlayer audioPlayer = new AudioPlayer();

        audioPlayer.play("mp3", "test1.mp3");
        audioPlayer.play("vlc", "test2.mp3");
        audioPlayer.play("mp4", "test3.mp3");
        audioPlayer.play("avi", "test4.mp3");
    }
}
