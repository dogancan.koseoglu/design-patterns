public class AudioPlayer implements MediPlayer {

    public void play(String audioType, String fileName) {

        if (audioType.equals("mp3")) {
            System.out.println("Play Mp3: " + fileName);
        } else if (audioType.equals("vlc") || audioType.equals("mp4")) {
            MediaAdapter mediaAdapter = new MediaAdapter(audioType);
            mediaAdapter.play(audioType, fileName);
        } else {
            System.out.println("Invalid Media: " + audioType);
        }

    }
}
