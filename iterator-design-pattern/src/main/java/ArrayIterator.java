public class ArrayIterator implements Iterator {

    Player[] team;

    int index = 0;

    public ArrayIterator(Player[] team) {
        this.setTeam(team);
    }

    public Player[] getTeam() {
        return team;
    }

    public void setTeam(Player[] team) {
        this.team = team;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean hasNext() {

        if (index >= getTeam().length || getTeam()[index] == null) {
            return false;
        }

        return true;
    }

    public Object next() {
        Player player = getTeam()[index];
        index++;
        return player;
    }
}
