public class FactoryOne {

    public void produceBluePencil() {
        System.out.println("Produce Blue Pencil");
    }

    public void produceBlueBook() {
        System.out.println("Produce Blue Book");
    }

}
