import java.util.ResourceBundle;

public class FileManager {

    private Strategy strategy;

    public static final FileManager fileManager = new FileManager();

    public Strategy getStrategy() {
        return strategy;
    }

    public void setStrategy(Strategy strategy) {
        this.strategy = strategy;
    }

    public static FileManager instance() {
        return fileManager;
    }

    public FileManager() {

        String strategyPropertyFile = ResourceBundle.getBundle("strategy").getString("strategy");

        try {
            setStrategy((Strategy) Class.forName(strategyPropertyFile).newInstance());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void saveBean(Bean bean) {
        getStrategy().save(bean);
    }
}
