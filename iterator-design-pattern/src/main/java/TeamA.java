import java.util.ArrayList;

public class TeamA implements Team {

    ArrayList<Player> playerList = new ArrayList<Player>();

    public TeamA() {
        playerList.add(new Player("a",1));
        playerList.add(new Player("b",2));
        playerList.add(new Player("c",3));
        playerList.add(new Player("d",4));
        playerList.add(new Player("e",5));

    }

    public ArrayList<Player> getPlayerList() {
        return playerList;
    }

    public void setPlayerList(ArrayList<Player> playerList) {
        this.playerList = playerList;
    }

    public Iterator getIterator() {
        return new ArrayListIterator(playerList);
    }
}
