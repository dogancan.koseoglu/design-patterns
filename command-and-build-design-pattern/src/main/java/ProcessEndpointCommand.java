import java.util.Date;

/**
 * author: dkoseoglu
 * date: 28.04.2021 21:38
 **/
public class ProcessEndpointCommand implements ProcessCommand {

    @Override
    public void execute(ProcessBuilder processBuilder) {
        processBuilder.getProcess().setDate(new Date());
        processBuilder.setEndpoint("test endpoint");
    }

}
