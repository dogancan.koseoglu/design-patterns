public class Person extends Decorator {

    public void create() {

        getUser().create();
        addPerson();
    }

    public void addPerson() {
        System.out.println("Add Person");
    }

}
