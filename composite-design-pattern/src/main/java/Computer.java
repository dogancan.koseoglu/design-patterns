import java.util.ArrayList;

public class Computer implements Item {

    private ArrayList<Item> itemList = new ArrayList<Item>();

    public void addItem(Item item) {
        getItemList().add(item);
    }

    public void removeItem(Item item) {
        if (getItemList().contains(item)) {
            getItemList().remove(item);
        }
    }

    public Item getItem(int index) {
        return getItemList().get(index);
    }

    public ArrayList<Item> getItemList() {
        return itemList;
    }

    public void setItemList(ArrayList<Item> itemList) {
        this.itemList = itemList;
    }

    public int getPrice() {

        int price = 0;

        for (int i = 0; i < getItemList().size(); i++) {
            price += getItemList().get(i).getPrice();
        }

        return price;
    }
}
