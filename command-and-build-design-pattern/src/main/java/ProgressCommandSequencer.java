import java.util.ArrayList;
import java.util.List;

/**
 * author: dkoseoglu
 * date: 28.04.2021 21:43
 **/

public class ProgressCommandSequencer {

    List<ProcessCommand> processCommandList = new ArrayList<>();

    public void takeCommand(ProcessCommand processCommand) {
        processCommandList.add(processCommand);
    }

    public void sequenceCommand(ProcessBuilder processBuilder) {
        for (ProcessCommand processCommand : processCommandList) {
            processCommand.execute(processBuilder);
        }
        processCommandList.clear();
    }

}
