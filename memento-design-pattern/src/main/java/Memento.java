import java.util.ArrayList;

public class Memento {

    Object[] elements;

    public Memento(Object[] elements) {
        this.elements = elements;
    }

    public Object[] getElements() {
        return elements;
    }

    public void setElements(Object[] elements) {
        this.elements = elements;
    }
}
