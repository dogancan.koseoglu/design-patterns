import java.util.ArrayList;
import java.util.List;

public class Nigde51 implements NewsPaper {

    List<Subscribe> subscribeList = new ArrayList<Subscribe>();

    public List<Subscribe> getSubscribeList() {
        return subscribeList;
    }

    public void setSubscribeList(List<Subscribe> subscribeList) {
        this.subscribeList = subscribeList;
    }

    public void addSubscribe(Subscribe subscribe) {
        getSubscribeList().add(subscribe);
    }

    public void removeScribe(Subscribe subscribe) {
        getSubscribeList().remove(subscribe);
    }

    public void sendInfoSubscribe() {
        for (int i = 0; i < getSubscribeList().size(); i++) {
            getSubscribeList().get(i).update();
        }
    }
}
