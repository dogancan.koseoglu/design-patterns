public class ProductionImplTwo implements Production {

    private FactoryTwo factoryTwo = new FactoryTwo();

    public void producePencil() {
        factoryTwo.produceRedPencil();
    }

    public void produceBook() {
        factoryTwo.produceRedBook();
    }
}
