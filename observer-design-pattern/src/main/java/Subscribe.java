public interface Subscribe {
    void update();

    void cancelSubscribe();

    void addSubscribe(NewsPaper newsPaper);
}
