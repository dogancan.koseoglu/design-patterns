public class TurnOnTvCommand implements Command {

    private TV tv = null;

    public TurnOnTvCommand(TV tv) {
        this.tv = tv;
    }

    public void execute() {
        this.tv.turnOnTv();
    }
}
