public class BridgePatternDemo {

    public static void main(String[] args) {

        Pencil pencil = new Pencil(new ProductionImplOne());
        pencil.produce();

        Book book = new Book(new ProductionImplOne());
        book.produce();

        Pencil pencilTwo = new Pencil(new ProductionImplTwo());
        pencilTwo.produce();

        Book bookTwo = new Book(new ProductionImplTwo());
        bookTwo.produce();

    }

}
