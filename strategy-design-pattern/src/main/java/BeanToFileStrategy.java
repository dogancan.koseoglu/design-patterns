import java.io.*;

public class BeanToFileStrategy implements Strategy {

    public void save(Bean bean) {


        try {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.append("counter: ").append(bean.getCounter()).append("\n")
                    .append("name: ").append(bean.getName());


            File file = new File(System.getProperty("user.dir") + "/src/bean.txt");

            if (file.exists()) {
                file.delete();
                file = new File(System.getProperty("user.dir") + "/src/bean.txt");
            }

            Writer writer = null;

            try {

                writer = new BufferedWriter(new FileWriter(file));
                writer.write(stringBuilder.toString());

            } finally {
                if (writer != null) {
                    writer.close();
                }
            }

        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
