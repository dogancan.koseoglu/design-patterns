/**
 * author: dkoseoglu
 * date: 28.04.2021 21:32
 **/
public class Main {

    public static void main(String[] args) {

        ProcessBuilder processBuilder = new ProcessBuilder(new Process());

        ProgressCommandSequencer progressCommandSequencer = new ProgressCommandSequencer();

        progressCommandSequencer.takeCommand(new ProcessIsActiveCommand());
        progressCommandSequencer.takeCommand(new ProcessEndpointCommand());

        progressCommandSequencer.sequenceCommand(processBuilder);

        System.out.println(processBuilder);

    }
}
