import java.util.ArrayList;

public class ArrayListIterator implements Iterator {

    ArrayList<Player> team;

    int index = 0;

    public ArrayListIterator(ArrayList<Player> team) {
        this.setTeam(team);
    }

    public ArrayList<Player> getTeam() {
        return team;
    }

    public void setTeam(ArrayList<Player> team) {
        this.team = team;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public boolean hasNext() {

        if (index >= getTeam().size() || getTeam().get(index) == null) {
            return false;
        }

        return true;
    }

    public Object next() {
        Player player = getTeam().get(index);
        index++;
        return player;
    }
}
