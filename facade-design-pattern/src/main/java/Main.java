public class Main {

    public static void main(String[] args) {

        FacadeShapeMaker facadeShapeMaker = FacadeShapeMaker.getFacadeShapeMaker();

        facadeShapeMaker.getFacade("c").draw();
        facadeShapeMaker.getFacade("s").draw();
        facadeShapeMaker.getFacade("r").draw();
    }
}
