/**
 * author: dkoseoglu
 * date: 28.04.2021 21:33
 **/
public class ProcessBuilder {

    private final Process process;

    private boolean isActive;

    private String endpoint;

    public ProcessBuilder(Process process) {
        this.process = process;
    }

    public Process getProcess() {
        return process;
    }

    public boolean isActive() {
        return isActive;
    }

    public ProcessBuilder setActive(boolean active) {
        isActive = active;
        return this;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public ProcessBuilder setEndpoint(String endpoint) {
        this.endpoint = endpoint;
        return this;
    }
}
