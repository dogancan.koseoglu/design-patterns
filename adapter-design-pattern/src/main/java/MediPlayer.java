public interface MediPlayer {
    public void play(String audioType, String fileName);
}
