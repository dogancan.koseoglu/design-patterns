public class Pencil extends Stock {

    public Pencil(Production production) {
        super(production);
    }

    public void produce() {
        getProduction().producePencil();
    }
}
